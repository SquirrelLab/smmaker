#include <boost/program_options.hpp>
#include <string>
#include <iostream>
#include <fstream>
using namespace std;
int main(int argc, char** argv)
{
  ofstream out;
  vector<string> opts, twoFold, option;
  string target, compiler, options;
  namespace po = boost::program_options;
  po::options_description desc("Options");
  desc.add_options()
    (",t", po::value<string>(), "The target file (Soft Requirement)")
    (",c", po::value<string>(), "Compiler Option (Soft Requirement)")
    (",d", po::value<string>(), "Directory Specification if one is needed")
    (",o", po::value<vector<string> >()->multitoken(), "Compiler Arguments")
    (",a", po::value<vector<string> >()->multitoken() ,"Additional files that will be put into the target file's section")
    ("help,h", "Get help" );
  po::variables_map vm;
  po::store(po::parse_command_line(argc, argv, desc), vm);
  po::notify(vm);
  if (vm.count("help") || vm.count("h")){
    cout << desc << "\n";
    return 1;
  }
  if (vm.count("-t")){
    target = vm["-t"].as<string>();
  }
  else{
    cout << "No target specified. Exiting";
    out.close();
    return 0;
  }
  if (vm.count("-c")){
    compiler = vm["-c"].as<string>();
  }
  else{
    cout << "No compiler specified. Exiting";
    return 0;
  }
    if (vm.count("-d")){
    out.open(vm["-d"].as<string>() + "Makefile");
  }
  else{
    out.open("Makefile");
  }
  if (vm.count("-o")){
    option = vm["-o"].as<vector<string> >();
  }
  if (vm.count("-a")){
    opts = vm["-a"].as<vector<string> >();
    for (int i = 0; i < opts.size(); i++){
      if (opts.at(i).find(".cpp") != string::npos){
	twoFold.push_back(opts.at(i));
      }
    }
  }
  string realTarget;
  if (target.find(".cpp") != string::npos){
    realTarget = target.substr(0, target.length() - 4);
  }
  else{
    realTarget = target.substr(0, target.length() - 2);
  }
  out << "CC = " << compiler << "\n";
  if (option.size() > 0){
    out << "CFLAGS = ";
    for (int i = 0; i < option.size(); i++){
      out << option.at(i) << " ";
    }
    out << "\n";
  }
  else{
    out << "CFLAGS = -v\n";
  }
  out << "TARGET = " << realTarget << "\n"
    "all: $(TARGET)\n"
    "$(TARGET): " << target << " ";
  for (int i = 0; i < opts.size(); i++)
    out << opts.at(i) << " ";					     
  out <<  "\n\t$(CC) $(CFLAGS) " << target << " ";
  for (int i = 0; i < twoFold.size(); i++)
    out << twoFold.at(i) << " ";
  out << " -o $(TARGET)\n"
    "clean:\n\t"
    "$(RM) $(TARGET)";
  
  out.close();
  return 0;
}
