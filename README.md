# smmaker

Simple Makefile Maker

smmaker is an easy to use utility to help build very simple test programs without having to worry about make formatting.
This tool is meant for one single target, and will not produce a clean looking Makefile if many files are included.
To build smmaker, a Makefile has been included, built from smmaker itself. This will give you an idea of the kind of
outputs that will be created with smmaker. To build the project then, all you have to do is run:

`make`

This project does depend on the boost c++ libraries. To find out all of the options available to you, run:

`smmaker -h` or `smmaker --help` to see all the options.

A man page has also been included with more descriptions and example usages of the program.