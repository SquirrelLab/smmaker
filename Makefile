CC = g++
CFLAGS = -lboost_program_options 
TARGET = smmaker
all: $(TARGET)
$(TARGET): smmaker.cpp 
	$(CC) $(CFLAGS) smmaker.cpp  -o $(TARGET)
clean:
	$(RM) $(TARGET)